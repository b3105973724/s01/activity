package com.zuitt.wdc044_s01.controllers;

import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMethod;


@RestController
//to indicate that a particular class is responsible for handling incoming HTTP requests and generating appropriate response
@CrossOrigin
// to enable Cross Origin resource sharing

public class PostController {
    @Autowired
    PostService postService;

    //Create a new post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost (@RequestHeader (value = "Authorization") String stringToken, @RequestBody Post post){
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

//Activity
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Post>> getPosts(){
        Iterable<Post> posts = postService.getPosts();
        return new ResponseEntity<>(posts, HttpStatus.OK);
    }

}
