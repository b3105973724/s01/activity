package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.models.Post;

public interface PostService {

    void createPost(String stringToken, Post post);
    //Now that we are generating JWT, ownership of a blog post will be retrieve from the JWTs payload
    Iterable<Post> getPosts();
}
